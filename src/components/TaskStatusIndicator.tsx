import React from "react";

import css from "./TaskStatusIndicator.module.scss";

interface Props {
  done: boolean;
  onClick: () => void;
}

const TaskStatusIndicator: React.FC<Props> = (props) => {
  const done = props.done;
  const onClick = props.onClick;
  return done ? (
    <div onClick={onClick} className={[css.TaskStatusIndicator, css.Done].join(" ")} />
  ) : (
    <div onClick={onClick} className={[css.TaskStatusIndicator].join(" ")} />
  );
};

export default TaskStatusIndicator;
