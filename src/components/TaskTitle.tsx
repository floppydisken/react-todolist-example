import React from "react";

import css from "./TaskTitle.module.scss";

interface Props {
  text: string;
  done: boolean;
}

const TaskTitle: React.FC<Props> = (props) => {
  const text = props.text;
  const done = props.done;

  return done ? (
    <div className={[css.TaskTitle, css.Done].join(" ")}>{text}</div>
  ) : (
    <div className={[css.TaskTitle].join(" ")}>{text}</div>
  );
};

export default TaskTitle;
