import React from "react";

import {IonIcon} from "@ionic/react";
import {happyOutline, skull} from "ionicons/icons";

import css from "./AddButton.module.scss";

interface Props {
  onClick: () => void;
  gothMode?: boolean;
}

const AddButton: React.FC<Props> = (props) => {
  const onClick = props.onClick;
  const gothMode = props.gothMode ?? false;

  return (
    <button onClick={onClick} className={css.AddButton}>
      {gothMode ? <IonIcon icon={skull} /> : <IonIcon icon={happyOutline} />}
    </button>
  );
};

export default AddButton;
