import React from "react";

import { Meta, Story } from "@storybook/react/types-6-0";
import Task, { TaskProps } from "./Task";

export default {
    title: "Task",
    component: Task,
} as Meta;

const Template: Story<TaskProps> = (props) => <Task {...props} />

export const Default = Template.bind({});
Default.args = {
    done: true,
    text: "Vaske tøj"
};