import React from "react";
import TaskStatusIndicator from "./TaskStatusIndicator";
import TaskTitle from "./TaskTitle";

import css from "./Task.module.scss";

interface Props {
  text: string;
  done: boolean;
  onComplete: () => void;
}

const Task: React.FC<Props> = (props) => {
  const text = props.text;
  const done = props.done;
  const onComplete = props.onComplete;

  return (
    <div className={css.Task}>

      <TaskTitle done={done} text={text} />

      <div className={css.RightColumn}>
        <TaskStatusIndicator done={done} onClick={onComplete} />
      </div>

    </div>
  );
};

export default Task;
export type {Props as TaskProps};
