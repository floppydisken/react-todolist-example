import React, {useState} from "react";
import AddButton from "./AddButton";
import TaskInput from "./TaskInput";

import css from "./BottomBar.module.scss";

interface Props {
  onAddTask: (taskName: string) => void;
  className?: string;
}

const BottomBar: React.FC<Props> = (props) => {
  const onAddTask = props.onAddTask;
  const className = props.className ?? "";
  const [inputValue, setInputValue] = useState("");
  const clearInput = () => setInputValue("");
  return (
    <div className={[css.BottomBar, className].join(" ")}>
      <TaskInput
        value={inputValue}
        onChange={(value) => setInputValue(value)}
      />
      <AddButton
        gothMode={false}
        onClick={() => {
          onAddTask(inputValue);
          clearInput();
        }}
      />
    </div>
  );
};

export default BottomBar;
export type {Props as BottomBarProps};
