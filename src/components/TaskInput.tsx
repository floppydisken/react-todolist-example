import React from "react";

import css from "./TaskInput.module.scss";

interface Props {
  onChange: (value: string) => void;
  value: string;
}

const TaskInput: React.FC<Props> = (props) => {
  const onChange = props.onChange;
  const value = props.value;

  return (
    <input
      onChange={(e) => onChange(e.target.value ?? "")}
      value={value}
      className={css.TaskInput}
      placeholder="Skriv task her..."
    />
  );
};

export default TaskInput;
