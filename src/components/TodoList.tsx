import React from "react";
import Task from "./Task";

import css from "./TodoList.module.scss";

class TaskData {
  constructor(public name: string, public isDone: boolean = false) {}
}

interface Props {
  tasks: TaskData[];
  onComplete: (task: TaskData) => void;
}

const TodoList: React.FC<Props> = (props) => {
  const tasks = props.tasks;
  const onComplete = props.onComplete;

  const wrappedTasks = tasks.map((taskData) => {
    return (
      <Task
        text={taskData.name}
        done={taskData.isDone}
        onComplete={() => {
          console.log("Clicked task with name " + taskData.name);
          onComplete(taskData);
        }}
      />
    );
  });

  return <div className={css.TodoList}>{wrappedTasks}</div>;
};

export default TodoList;
export {TaskData};
export type {Props as TodoListProps};
