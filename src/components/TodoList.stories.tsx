import React from "react";

import { Meta, Story } from "@storybook/react/types-6-0";
import Task, { TaskProps } from "./Task";
import TodoList, { TaskData, TodoListProps } from "./TodoList";

export default {
    title: "TodoList",
    component: TodoList,
} as Meta;

const Template: Story<TodoListProps> = (props) => <TodoList {...props} />

export const Default = Template.bind({});
Default.args = {
    tasks: [
        new TaskData("Vaske tøj"),
        new TaskData("Se dank memes"),
        new TaskData("Bombardere Bornholm", true)
    ]
};