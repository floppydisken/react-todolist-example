import React from "react";

import { Meta, Story } from "@storybook/react/types-6-0";
import Home from "./Home";

export default {
    title: "Home",
    component: Home,
} as Meta;

const Template: Story = (props) => <Home {...props} />

export const Default = Template.bind({});