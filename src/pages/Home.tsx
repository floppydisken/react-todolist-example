import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, {Dispatch, SetStateAction, useEffect, useState} from "react";
import BottomBar from "../components/BottomBar";
import TodoList, {TaskData} from "../components/TodoList";
import css from "./Home.module.scss";

function useArray<T>(
  initialArray: T[]
): [T[], Dispatch<SetStateAction<T[]>>, () => void] {
  const [update, setUpdate] = useState(false);
  const [array, setArray] = useState(initialArray);

  useEffect(() => {
    if (update) 
      setUpdate(false);
  }, [update]);

  return [array, setArray, () => setUpdate(true)];
}

const Home: React.FC = () => {
  const [tasks, setArray, updateArray] = useArray<TaskData>([
    new TaskData("Vaske tøj"),
    new TaskData("Se dank memes"),
    new TaskData("Bombardere Bornholm", true),
  ]);

  return (
    <div className={css.Home}>
      <h1>TODOLIST APP</h1>
      <TodoList
        tasks={tasks}
        onComplete={(task) => {
          task.isDone = !task.isDone;
          updateArray();
          console.log("Task " + task.name + " is done");
        }}
      />
      <div className={css.Fade} />
      <BottomBar className={css.BottomBar} onAddTask={(value) => {
        tasks.push(new TaskData(value));
        updateArray();
      }}/>
    </div>
  );
};

export default Home;
